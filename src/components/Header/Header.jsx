export default function Header({name}) {
    
    
    return(
        <header>
            <h1>My TodoList!</h1>
            <h2>Created by { name }</h2>
        </header>
    )
}