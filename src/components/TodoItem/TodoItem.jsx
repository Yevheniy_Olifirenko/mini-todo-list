export default function TodoItem({ todos, handleClick }) {


    return(
        <li style={{ textDecoration: todos.status==="done" ? "line-through" : "", cursor: "pointer" }} onClick={ () => handleClick(todos.id)}>
            {todos.title}
        </li>
    )
}