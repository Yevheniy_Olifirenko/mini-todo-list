import TodoItem from "../TodoItem/TodoItem";

export default function TodoList({todo, handleClick}) {
    
    console.log("todo", todo);
    return(
        <ul>
            {todo.map((todos) => <TodoItem key={todos.id} todos={todos} handleClick={handleClick} />)}
        </ul>
    )
}

// function calc() {
//     const result = todo[0].status === "done" ? console.log("do") : null;
// }