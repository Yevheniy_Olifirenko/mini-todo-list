import Header from "./components/Header/Header"
import TodoList from "./components/TodoList/TodoList";
import { useState } from "react";

function App() {

  const myName = "Zhenia O.";

  const [todo, setTodos] = useState ([
    {id: "1", title: "Learn Components", status: "done"},
    {id: "2", title: "Learn Props", status: "undone"},
    {id: "3", title: "Learn Arrays", status: "undone"}
  ]);

  function handleClick(id) {
    setTodos(prevState => {
      const itemToChange = todo.find(todos => todos.id === id);
      const itemToChangeIndex = todo.findIndex(todos => todos.id === id);
      const newTodo = {
        ...itemToChange,
        status: itemToChange.status === "done" ? "undone" : "done",
      }
      const newTodos = [
        ...prevState.slice(0, itemToChangeIndex),
        newTodo, 
        ...prevState.slice(itemToChangeIndex + 1)
      ]
      return newTodos;
    })
  }


  return (
    <>
      <Header name={myName} />
      <TodoList todo={todo} handleClick={handleClick} />
    </>
  )
}

export default App
